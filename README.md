
# SCCA (SAP CX CLI)
This tool utilizes the SAP Commerce cloud APIs for building and deploying to the SAP Commerce Cloud.

Usage
You can run the following command to see what the availble options are

./scca -h

Usage:

scca config set --api-token=<token> --sap-subscription-id=<subscriptionId>

scca config show

scca build create --branch=<branch> --name=<name> [--no-wait]

scca build progress --build-code=<build-code>

scca build deploy --build-code=<build-code> --environment-code=<environment-code> [--database-update-mode=<update-mode>] [--strategy=<strategy>] [--no-wait]

scca deploy progress --build-code=<build-code>

scca build code --branch=<branch>

scca build test --branch=<branch> --environment-code=<environment-code> [--database-update-mode=<update-mode>] [--strategy=<strategy>]

Options:
-h --help Show this screen.

--version Show version.

--strategy=<strategy> The way the deployment is handled [default: ROLLING_UPDATE]

--database-update-mode=<update-mode> DB update mode [default: NONE]